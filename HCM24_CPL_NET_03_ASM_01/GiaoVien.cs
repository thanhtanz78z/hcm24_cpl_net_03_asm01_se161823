﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCM24_CPL_NET_03_ASM_01
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien() { }
        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(float heSoLuong)
        {
            HeSoLuong = heSoLuong;

        }
        public override double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine("He so luong la: " + HeSoLuong);
            Console.WriteLine("Luong la: " + TinhLuong());

        }
        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}
